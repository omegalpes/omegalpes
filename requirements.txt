OMEGAlpes >= 0.3.1
PuLP >= 2.6
Matplotlib >= 2.2.2
Numpy >= 1.14.2
Pandas >= 0.25.0
lpfics >= 0.0.1