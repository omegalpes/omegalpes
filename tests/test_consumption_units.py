#! usr/bin/env python3
#  -*- coding: utf-8 -*-

""" Unit tests for consumption_units module

..

    Copyright 2018 G2ELab / MAGE

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

import unittest
from omegalpes.general.time import TimeUnit
from omegalpes.energy.units.consumption_units import ConsumptionUnit, \
    FixedConsumptionUnit, VariableConsumptionUnit


class TestInitConsumptionUnit(unittest.TestCase):
    """ Check the initialisation of the ConsumptionUnit class"""

    def test_none_definition(self):
        """ Check if the external constraints are none at the initialisation """
        cu0 = ConsumptionUnit(time=TimeUnit(periods=24, dt=1), name='CU0')

        self.assertIsNone(cu0.set_max_ramp_up)
        self.assertIsNone(cu0.set_max_ramp_down)
        self.assertIsNone(cu0.set_min_up_time)
        self.assertIsNone(cu0.set_min_down_time)
        self.assertIsNone(cu0.set_availability)


class TestDefConsumptionCostCalc(unittest.TestCase):
    """ Check the _def_consumption_cost_calc method """

    def test_none_consumption_cost_calc(self):
        """ check if the consumption_cost is none """
        cu0 = ConsumptionUnit(time=TimeUnit(periods=24, dt=1), name='CU0')
        self.assertIsNone(cu0.operating_cost)

    def test_int_consumption_cost_calc(self):
        """ check the cost calculation if consumption_cost is an int """
        cu_cost_int = ConsumptionUnit(time=TimeUnit(periods=24, dt=1),
                                      consumption_cost=3, name='CU0')
        self.assertEqual(cu_cost_int.calc_operating_cost.exp,
                         'CU0_operating_cost[t] == 3 * CU0_p[t] * time.DT '
                         'for t in time.I')

    def test_float_consumption_cost_calc(self):
        """ check the cost calculation if consumption_cost is a float """
        cu_cost_float = ConsumptionUnit(time=TimeUnit(periods=24, dt=1),
                                        consumption_cost=3.5, name='CU0')
        self.assertEqual(cu_cost_float.calc_operating_cost.exp,
                         'CU0_operating_cost[t] == 3.5 * CU0_p[t] * time.DT '
                         'for t in time.I')

    def test_list_consumption_cost_calc(self):
        """ check the cost calculation if consumption_cost is a list
        First an incomplete list considering the time of the study
        Then a complete list
        """
        with self.assertRaises(IndexError):
            ConsumptionUnit(time=TimeUnit(periods=24, dt=1), name='CU0',
                            consumption_cost=[1, 2])

        cu_cost_float = ConsumptionUnit(time=TimeUnit(periods=2, dt=1),
                                        consumption_cost=[0, 1], name='CU0')
        self.assertEqual(cu_cost_float.calc_operating_cost.exp,
                         'CU0_operating_cost[t] == [0, 1][t] * CU0_p[t] * '
                         'time.DT for t in time.I')

    def test_dict_consumption_cost_calc(self):
        """ check no cost calculation if consumption_cost is a dictionary """
        with self.assertRaises(TypeError):
            ConsumptionUnit(time=TimeUnit(periods=24, dt=1), name='CU0',
                            consumption_cost={0: 1})


class TestMinimizeConsumption(unittest.TestCase):
    """ Check the minimize_consumption method """

    def setUp(self):
        self.cu0 = ConsumptionUnit(TimeUnit(periods=24, dt=1), name='CU0')
        self.cu0.minimize_consumption()

    def test_minimize_consumption_name(self):
        """ Check the name of the min_consumption objective """
        self.assertIs(self.cu0.min_energy.name,
                      'min_consumption')

    def test_minimize_consumption_expression(self):
        """ Check the expression of the min_consumption objective """
        self.assertEqual(self.cu0.min_energy.exp,
                         'lpSum(CU0_p[t] for t in time.I)')


class TestMaximizeConsumption(unittest.TestCase):
    """ Check the maximize_consumption method """

    def setUp(self):
        self.cu0 = ConsumptionUnit(TimeUnit(periods=24, dt=1), name='CU0')
        self.cu0.maximize_consumption()

    def test_minimize_consumption_name(self):
        """ Check the name of the max_consumption objective """
        self.assertIs(self.cu0.min_energy.name,
                      'max_consumption')

    def test_minimize_consumption_expression(self):
        """ Check the expression of the max_consumption objective """
        self.assertEqual(self.cu0.min_energy.exp,
                         'lpSum(CU0_p[t] for t in time.I)')
        self.assertEqual(self.cu0.min_energy.weight, -1)


class TestMinimizeConsumptionCost(unittest.TestCase):
    """ Check the minimize_consumption_cost method """

    def setUp(self):
        self.cu0 = VariableConsumptionUnit(TimeUnit(periods=24, dt=1),
                                           name='CU0')
        self.cu0._add_operating_cost(3)
        self.cu0.minimize_consumption_cost()

    def test_minimize_consumption_name(self):
        """ Check the name of the min_consumption_cost objective """
        self.assertIs(self.cu0.min_operating_cost.name,
                      'min_consumption_cost')

    def test_minimize_consumption_expression(self):
        """ Check the expression of the min_consumption_cost objective """
        self.assertEqual(self.cu0.min_operating_cost.exp,
                         'lpSum(CU0_operating_cost[t] for t in time.I)')


class TestFixedConsumptionUnit(unittest.TestCase):
    """ Check the FixedConsumptionUnit class """

    def test_variable_consumption_none_p(self):
        """ Check if it raises an error if no energy profile is done for a
        FixedConsumptionUnit """
        with self.assertRaises(TypeError):
            FixedConsumptionUnit(TimeUnit(periods=24, dt=1))
