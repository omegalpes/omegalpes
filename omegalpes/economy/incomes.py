#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""

..
    Copyright 2018 G2Elab / MAGE

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

from omegalpes.economy.economy import EconomicObject
from omegalpes.energy.units.energy_units import EnergyUnit
from omegalpes.general.optimisation.elements import *
from omegalpes.economy.economy import *


class Incomes(EconomicObject):
    def __init__(self, time, name, unit):
        EconomicObject.__init__(self, time=time, name=name, unit=unit)

        self.description = 'External incomes'

        if isinstance(unit, EnergyUnit):

            unit.incomes_t = Quantity(name='incomes_t', unit='€',
                                      vlen=time.LEN, parent=unit)

            unit.incomes_tot = Quantity(name='incomes_tot', unit='€',
                                        vlen=1, parent=unit)

        else:
            raise TypeError("Incomes should be added to an energy unit "
                            "and is added instead to "
                            "a {}".format(type(unit)))


class EnergyUnitIncomes(Incomes):
    def __init__(self, time, name,
                 energy_unit,
                 energy_incomes=None,
                 grant_value=None,
                 grant_spread_duration=None
                 ):
        Incomes.__init__(self, time=time, name=name, unit=energy_unit)

        self.description = 'External incomes linked to an energy unit'

        # Operating cost
        if energy_incomes is not None:
            self.add_energy_incomes(time=time, name='energy_incomes',
                                    energy_incomes=energy_incomes,
                                    energy_unit=energy_unit)
            self.add_income_to_incomes_exp(
                unit=energy_unit,
                income_name=energy_unit.energy_incomes.name)

        # Investment
        if grant_value is not None:
            if grant_spread_duration is not None:
                self.add_spread_grant(
                    time=time,
                    name='spread_grant',
                    grant_value=grant_value,
                    spread_duration=grant_spread_duration,
                    energy_unit=energy_unit)
                self.add_income_to_incomes_exp(
                    unit=energy_unit,
                    income_name=energy_unit.spread_grant.name)

            else:
                self.add_grant(time=time, name='grant',
                               grant_value=grant_value,
                               energy_unit=energy_unit)
                self.add_income_to_incomes_exp(
                    unit=energy_unit,
                    income_name=energy_unit.grant.name)

        self.calc_incomes(unit=energy_unit)

    def add_energy_incomes(self, time, name, energy_incomes, energy_unit):
        """
            Add an incomes linked to the energy produced or consumed by the
            energy unit
        """
        self.add_quantity_dependant_cashflow(time=time, name=name,
                                             description='energy incomes',
                                             value=energy_incomes,
                                             quantity_name='p',
                                             quantity_unit=energy_unit,
                                             unit=energy_unit)

    def add_grant(self, time, name, grant_value, energy_unit):
        """
            Add grant to the energy unit

        **Parameters**
            - grant_value: int or float: value of the investment to consider

        """
        self.add_cashflow(time=time, name=name,
                          description='basic grant',
                          cashflow_value=grant_value,
                          energy_unit=energy_unit)

    def add_spread_grant(self, time, name, grant_value, spread_duration,
                         energy_unit):
        """
        Add spead grant

        **Parameters**
            - investment_value: float: value of the investment to consider
            - spread_duration: int: duration over which the investment will be
            spread, the whole duration of the study is considered by definition

        """
        self.add_spread_cashflow(time=time, name=name,
                                 description='spread grant',
                                 cashflow_value=grant_value,
                                 spread_duration=spread_duration,
                                 energy_unit=energy_unit)
