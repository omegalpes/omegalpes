#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""

..
    Copyright 2018 G2Elab / MAGE

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

from omegalpes.economy.economy import EconomicObject
from omegalpes.energy.units.energy_units import EnergyUnit
from omegalpes.general.optimisation.elements import *


class Costs(EconomicObject):
    def __init__(self, time, name, unit):
        EconomicObject.__init__(self, time=time, name=name, units=unit)

        self.description = 'External costs'

        if isinstance(unit, EnergyUnit):

            unit.costs_t = Quantity(name='costs_t', unit='€',
                                    vlen=time.LEN, parent=unit)

            unit.costs_tot = Quantity(name='costs_tot', unit='€',
                                      vlen=1, parent=unit)

            unit.operation_and_maintenance_costs_t = Quantity(
                name='operation_and_maintenance_costs_t', unit='€',
                vlen=time.LEN, parent=unit)

            unit.operation_and_maintenance_costs_tot = Quantity(
                name='operation_and_maintenance_costs_tot', unit='€',
                vlen=1, parent=unit)

        else:
            raise TypeError("Cost should be added to an energy unit "
                            "and is added instead to "
                            "a {}".format(type(unit)))


class EnergyUnitCosts(Costs):
    def __init__(self, time, name,
                 energy_unit,
                 energy_cost=None,
                 operation_starting_cost=None,
                 operation_switch_off_cost=None,
                 operation_fixed_cost=None,
                 maintenance_fixed_cost=None,
                 maintenance_investment_rate_cost=None,
                 investment_value=None,
                 investment_spread_duration=None):

        Costs.__init__(self, time=time, name=name, unit=energy_unit)

        self.name = name

        self.description = 'External costs linked to an energy unit'

        # Energy cost during the operation
        if energy_cost is not None:
            self.add_quantity_dependant_cashflow(time=time, name='energy_cost',
                                                 description='energy cost',
                                                 value=energy_cost,
                                                 quantity_name='p',
                                                 quantity_unit=energy_unit,
                                                 unit=energy_unit)
            self.add_cost_to_costs_exp(
                unit=energy_unit,
                cost_name=energy_unit.energy_cost.name)

        # Starting cost
        if operation_starting_cost is not None:
            self.add_starting_costs(time=time, name='starting_cost',
                                    start_cost=operation_starting_cost,
                                    energy_unit=energy_unit)
            self.add_cost_to_costs_exp(
                unit=energy_unit,
                cost_name=energy_unit.starting_cost.name)

            self.add_cost_to_operation_and_maintenance_exp(
                unit=energy_unit,
                cost_name=energy_unit.starting_cost.name)

        if operation_switch_off_cost is not None:
            self.add_switch_off_costs(time=time, name='switch_off_cost',
                                      switch_off_cost=operation_switch_off_cost,
                                      energy_unit=energy_unit)
            self.add_cost_to_costs_exp(
                unit=energy_unit,
                cost_name=energy_unit.switch_off_cost.name)
            self.add_cost_to_operation_and_maintenance_exp(
                unit=energy_unit,
                cost_name=energy_unit.switch_off_cost.name)

        if operation_fixed_cost is not None:
            self.add_cashflow(time=time, name='operation_fixed_cost',
                              description='operation fixed costs',
                              cashflow_value=operation_fixed_cost,
                              unit=energy_unit)
            self.add_cost_to_costs_exp(
                unit=energy_unit,
                cost_name=energy_unit.operation_fixed_cost.name)
            self.add_cost_to_operation_and_maintenance_exp(
                unit=energy_unit,
                cost_name=energy_unit.operation_fixed_cost.name)

        if maintenance_fixed_cost is not None:
            self.add_cashflow(time=time, name='maintenance_fixed_cost',
                              description='maintenance fixed costs',
                              cashflow_value=maintenance_fixed_cost,
                              unit=energy_unit)
            self.add_cost_to_costs_exp(
                unit=energy_unit,
                cost_name=energy_unit.maintenance_fixed_cost.name)
            self.add_cost_to_operation_and_maintenance_exp(
                unit=energy_unit,
                cost_name=energy_unit.maintenance_fixed_cost.name)

        if maintenance_investment_rate_cost is not None:
            if investment_value is None:
                raise ValueError('There is no value for the parameter '
                                 'investment_value, however the '
                                 'maintenance_investment_rate_cost depends '
                                 'on the investment_value')
            else:

                self.add_rate_cashflow(time=time,
                                       name='maintenance_investment_rate_cost',
                                       description='maintenance cost based '
                                                   'on a rate of the '
                                                   'investment',
                                       rate_value=
                                       maintenance_investment_rate_cost,
                                       cashflow_value=investment_value,
                                       unit=energy_unit)
                self.add_cost_to_costs_exp(
                    unit=energy_unit,
                    cost_name=energy_unit.maintenance_investment_rate_cost
                        .name)
                self.add_cost_to_operation_and_maintenance_exp(
                    unit=energy_unit,
                    cost_name=energy_unit.maintenance_investment_rate_cost
                        .name)
        # Investment
        if investment_value is not None:
            if investment_spread_duration is not None:
                self.add_spread_cashflow(time=time, name='spread_investment',
                                         description='spread investment',
                                         cashflow_value=investment_value,
                                         spread_duration=
                                         investment_spread_duration,
                                         unit=energy_unit)
                self.add_cost_to_costs_exp(
                    unit=energy_unit,
                    cost_name=energy_unit.spread_investment.name)

            else:
                self.add_cashflow(time=time, name='investment',
                                  description='basic investment costs',
                                  cashflow_value=investment_value,
                                  unit=energy_unit)
                self.add_cost_to_costs_exp(
                    unit=energy_unit,
                    cost_name=energy_unit.investment.name)

        self.calc_costs(unit=energy_unit)
        self.calc_operation_and_maintenance_costs(unit=energy_unit)

    def add_starting_costs(self, time, name, start_cost, energy_unit):
        """
            Add a starting cost to the energy unit
        """
        if energy_unit.start_up is None:
            energy_unit._add_start_up()

        self.add_quantity_dependant_cashflow(time=time, name=name,
                                             description='start up cost',
                                             value=start_cost,
                                             quantity_name='start_up',
                                             quantity_unit=energy_unit,
                                             unit=energy_unit)

    def add_switch_off_costs(self, time, name, switch_off_cost, energy_unit):
        """
            Add a switch off costs to the energy unit
        """
        if energy_unit.switch_off is None:
            energy_unit._add_switch_off()

        self.add_quantity_dependant_cashflow(time=time, name=name,
                                             description='switch off cost',
                                             value=switch_off_cost,
                                             quantity_name='switch_off',
                                             quantity_unit=energy_unit,
                                             unit=energy_unit)
