Authors (alphabetical order):
=============================

- Gilles DEBIZET

- Benoit DELINCHANT

- Mathieu BRUGERON

- Jaume FITO

- Sacha HODENCQ

- Yves MARECHAL

- Lou MORRIET

- Camille PAJOT

- Julien RAMOUSSE

- Vincent REINBOLD

- Benoit STUTZ

- Frédéric WURTZ

To see the details of the contributions, see the `Contributors in OMEGAlpes repository`_.

You can also check `OMEGAlpes publications`_.

.. _`Contributors in OMEGAlpes repository`: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes/-/graphs/master
.. _`OMEGAlpes publications`: https://omegalpes.readthedocs.io/en/latest/api/publications.html