general package
===============

general.optimisation.elements module
------------------------------------

.. automodule:: general.optimisation.elements
    :members:
    :undoc-members:
    :show-inheritance:

general.optimisation.model module
---------------------------------

.. automodule:: general.optimisation.model
    :members:
    :undoc-members:
    :show-inheritance:

general.optimisation.units module
---------------------------------

.. automodule:: general.optimisation.units
    :members:
    :undoc-members:
    :show-inheritance:

general.plots module
--------------------

.. automodule:: general.plots
    :members:
    :undoc-members:
    :show-inheritance:

general.time module
-------------------

.. automodule:: general.time
    :members:
    :undoc-members:
    :show-inheritance:
