.. OMEGALPES documentation master file, created by
   sphinx-quickstart on Thu Jul  5 20:04:12 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OMEGALPES's documentation!
=====================================


Introduction to OMEG'ALPES
--------------------------

OMEG'ALPES stands for Generation of Optimization Models As Linear Programming for Energy Systems.

OMEG'ALPES aims to be an energy systems modelling tool for linear optimisation (LP, MILP).
It is currently based on the LP modeler PuLP.

Contents
--------
.. toctree::
   :maxdepth: 2

   installation_requirements

.. toctree::
   :maxdepth: 1

   OMEGALPES_description


.. toctree::
   :maxdepth: 2

   examples


Acknowledgments
---------------
Vincent Reinbold - Library For Linear Modeling of Energetic Systems : https://github.com/ReinboldV

Mathieu Brugeron

This work has been partially supported by the `CDP Eco-SESA`_ receiving fund from the French National Research Agency
in the framework of the "Investissements d’avenir” program (ANR-15-IDEX-02) and the VALOCAL project
(CNRS Interdisciplinary Mission and INSIS)

.. _CDP Eco-SESA: https://ecosesa.univ-grenoble-alpes.fr/

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
