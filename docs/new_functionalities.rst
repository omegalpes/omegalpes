What's new in the latest versions
=================================

The new version of OMEGAlpes v0.4.4 is available!
The release is from 20th of January 2023.

.. toctree::
   :maxdepth: 2

   new_functionalities/v0.4.4


Information about the latest versions
-------------------------------------


.. toctree::
   :maxdepth: 2

   new_functionalities/v0.4.3


.. toctree::
   :maxdepth: 1

   new_functionalities/v0.4.2

.. toctree::
   :maxdepth: 1

   new_functionalities/v0.4.1

.. toctree::
   :maxdepth: 1

   new_functionalities/v0.4.0

.. toctree::
   :maxdepth: 1

   new_functionalities/v0.3.1

.. toctree::
   :maxdepth: 1

   new_functionalities/v0.3.0
