What’s new in version 0.4.3
===========================
Version available since April 29, 2022

Bug fixed
---------
Model
+++++
Changed indexes into indices due to PuLP update causing errors.

Documentation
-------------
- Updating the authors in the README, docs and authors, and adding partners in the development of OMEGAlpes.

Contributors
------------

Sacha Hodencq,
Mathieu Brugeron,