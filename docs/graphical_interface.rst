Graphical interface
===================

*This page is under development and wil be updated*

A graphical interface of OMEGAlpes has been developed in order to be able
to build its model. The graphical interface currently does not include
latest fucntions such as reversible energy units, and only enable to create
an OMEGAlpes script, further developments are underway.

The graphical interface of OMEGAlpes is available at this adress:
https://mhi-srv.g2elab.grenoble-inp.fr/OMEGAlpes-web-front-end/

The documentation linked to this interface is available in French in the
gitlab wiki of OMEGAlpes-web_back-end:
https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes-web-back-end/-/wikis/home

and OMEGAlpes-web-front-end:
https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes-web-front-end/-/wikis/home
