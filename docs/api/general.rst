general package
===============

Please, have a look to the following general modules

.. contents::
    :depth: 1
    :local:
    :backlinks: top

**Firstly, the main modules to build an energy optimisation
model are presented :**

Elements module
---------------

.. automodule:: omegalpes.general.optimisation.elements
    :members:
    :undoc-members:
    :show-inheritance:

Core module
------------

.. automodule:: omegalpes.general.optimisation.core
    :members:
    :undoc-members:
    :show-inheritance:

Time module
-----------

.. automodule:: omegalpes.general.time
    :members:
    :undoc-members:
    :show-inheritance:

Model module
------------

.. automodule:: omegalpes.general.optimisation.model
    :members:
    :undoc-members:
    :show-inheritance:


**Then, utils methods are developped and are presented in the
following module:**

Input Data module
-----------------

.. automodule:: omegalpes.general.utils.input_data
    :members:
    :undoc-members:
    :show-inheritance:

Output Data module
------------------

.. automodule:: omegalpes.general.utils.output_data
    :members:
    :undoc-members:
    :show-inheritance:

Plots module
------------

.. automodule:: omegalpes.general.utils.plots
    :members:
    :undoc-members:
    :show-inheritance:

Maths module
------------

.. automodule:: omegalpes.general.utils.maths
    :members:
    :undoc-members:
    :show-inheritance:
