OMEGAlpes publications
======================

OMEGAlpes was presented in details in the following publications:

- In the 2021 *energies* article `OMEGAlpes, an Open-Source Optimisation Model Generation Tool to Support Energy Stakeholders at District Scale`_, where the tool and its associated use cases are presented in detail.

- In 2019 in the *Building Simulation* conference article `An Optimization Modeler as an Efficient Tool for Design and Operation for City Energy Stakeholders and Decision Makers`_

- In French in 2019 in Camille Pajot PhD thesis `OMEGAlpes Outil d'aide à la décision pour une planification énergétique multi-fluides optimale à l'échelle des quartiers`_

The tool was also used in various scientific works:

- Pajot, C.; Delinchant, B.; Marechal, Y.; Wurtz, F.; Morriet, L.; Vincent, B.; Debray, F. Industrial Optimal Operation Plan-ning with Financial and Ecological Objectives. In Proceedings of the 7th International Conference on Smart Cities and Green ICT Systems; Funchal, Portugal, 16–18 March 2018; SciTePress—Science and and Technology Publications, Setúbal, Portugal, 2018; pp. 214–222.

- Pajot, C.; Nguyen, Q.; Delinchant, B.; Maréchal, Y.; Wurtz, F.; Robin, S.; Vincent, B.; Debray, F. Data-driven Modeling of Building Consumption Profile for Optimal Flexibility: Application to Energy Intensive Industry. In Proceedings of the Building Simulation Conference, Rome, Italy, 2–4 September 2019. Available online: https://hal.archives-ouvertes.fr/hal-02364669 (accessed on 17 December 2019).

- Hodencq, S.; Debray, F.; Trophime, C.; Vincent, B.; Stutz, B.; Delinchant, B. Thermohydraulics of High Field Magnets: From microns to urban community scale. In Proceedings of the 24ème Congrès Français de Mécanique, Brest, France, 26–30 August 2019.

- Pajot, C.; Artiges, N.; Delinchant, B.; Rouchier, S.; Wurtz, F.; Maréchal, Y. An Approach to Study District Thermal Flexibility Using Generative Modeling from Existing Data. Energies 2019, 12, 3632, doi:10.3390/en12193632.

- Morriet, L.; Debizet, G.; Wurtz, F. Multi-Actor Modelling for MILP Energy Systems Optimisation: Application to Collective Self-Consumption. In Proceedings of the Building Simulation 2019: 16th Conference of IBPSA,  Rome, Italy, 2–4 September 2019. https://hal.archives-ouvertes.fr/hal-02285965

- Fitó, J.; Ramousse, J.; Hodencq, S.; Wurtz, F. Energy, exergy, economic and exergoeconomic (4E) multicriteria analysis of an industrial waste heat valorization system through district heating. Sustain. Energy Technol. Assess. 2020, 42, 100894, doi:10.1016/j.seta.2020.100894

- Fitó, J.; Hodencq, S.; Ramousse, J.; Wurtz, F.; Stutz, B.; Debray, F.; Vincent, B. Energy- and exergy-based optimal designs of a low-temperature industrial waste heat recovery system in district heating. Energy Convers. Manag. 2020, 211, 112753, doi:10.1016/j.enconman.2020.112753.

- Fitó, J.; Ramousse, J.; Hodencq, S.; Morriet, L.; Wurtz, F.; Debizet, G. Analyse technico-économique multi-acteurs de la conception d’un système de valorisation de chaleur fatale sur réseau de chaleur. In Proceedings of the Communautés Énergétiques, Autoproduction, Autoconsommation: Cadrages, Pratiques et Outils, Paris, France, 16 June 2020; p. 13.

- Hodencq, S.; Morriet, L.; Wurtz, F.; Delinchant, B.; Vincent, B.; Debray, F. Science ouverte pour l’optimisation de systèmes énergétiques: Des données et modèles ouverts à une infrastructure de recherche ouverte. In Proceedings of the Conférence IBPSA, Reims, France, 13–14 May 2020. Available online: https://hal.archives-ouvertes.fr/hal-03290009 (ac-cessed on 23 July 2021).

- Hodencq, S.; Fitó, J.; Debray, F.; Vincent, B.; Ramousse, J.; Delinchant, B.; Wurtz, F. Flexible waste heat management and recovery for an electro-intensive industrial process through energy/exergy criteria. In Proceedings of the Ecos 2021-The 34rth International Conference on Efficiency, Cost, Optimization, Simulation and Environmental Impact of Energy Systems, Taormina, Italy, 28 Jun–2 July 2021. Available online: https://hal.archives-ouvertes.fr/hal-03290126 (accessed on 23 July 2021).

- Hodencq, S.; Delinchant, B.; Frederic, W.; Artiges, N.; Ferrari, J.; Laranjeira, T.; Morriet, L.; Benjamin, V.; Francois, D. Towards an energy open science approach at district level: Application to Grenoble Presqu’île’. In Proceedings of the 1st International Workshop on Open Design & Open Source Hardware Product Development, Grenoble, France, 5–6 March 2020. Available online: https://hal.archives-ouvertes.fr/hal-03052326 (accessed on 1 March 2021).

- Hodencq, S.; Delinchant, B.; Wurtz, F., « Open and Reproducible Use Cases for Energy (ORUCE) methodology in systems design and operation: a dwelling photovoltaic self-consumption example », In Proceedings of the Building Simulation 2021: 17th Conference of IBPSA, Bruges, Belgium, 1 - 3 sept. 2021. https://hal.archives-ouvertes.fr/hal-03341883


.. _OMEGAlpes, an Open-Source Optimisation Model Generation Tool to Support Energy Stakeholders at District Scale: https://www.mdpi.com/1996-1073/14/18/5928
.. _An Optimization Modeler as an Efficient Tool for Design and Operation for City Energy Stakeholders and Decision Makers: https://hal.archives-ouvertes.fr/hal-02285954
.. _OMEGAlpes Outil d'aide à la décision pour une planification énergétique multi-fluides optimale à l'échelle des quartiers: https://www.theses.fr/s162247
