Welcome to OMEGAlpes' documentation!
=====================================


Introduction to OMEGAlpes
--------------------------

OMEGAlpes stands for Generation of Optimization Models As Linear Programming for Energy Systems.
It aims to be an energy systems modelling tool for linear optimisation (LP, MILP).
It is currently based on the LP modeler PuLP.

It is an Open Source project located on GitLab at `OMEGAlpes Gitlab`_
An associated web interface is available at `OMEGAlpes interface`_ to help
generate scripts

Contents
--------
.. toctree::
   :maxdepth: 2

   about_OMEGAlpes

.. toctree::
   :maxdepth: 1

   installation_requirements

.. toctree::
   :maxdepth: 2

   OMEGAlpes_description

.. toctree::
   :maxdepth: 1

   OMEGAlpes_graph_representation

.. toctree::
   :maxdepth: 1

   how_to

.. toctree::
   :maxdepth: 1

   graphical_interface

.. toctree::
   :maxdepth: 1

   new_functionalities


.. toctree::
   :maxdepth: 1

   examples

Licence
-------
OMEGAlpes is open-source and written in Python with the licence `Apache 2.0`_.
This documentation is licensed with the `Creative Commons Attribution 4.0 International (CC BY 4.0)`_

Authors:
--------
See `AUTHORS in OMEGAlpes repository`_.

Acknowledgments
---------------
Vincent Reinbold - Library For Linear Modeling of Energetic Systems : https://github.com/ReinboldV

This work has been partially supported by the `CDP Eco-SESA`_ receiving fund from the French National Research Agency
in the framework of the "Investissements d’avenir” program (ANR-15-IDEX-02) and the VALOCAL project
(CNRS Interdisciplinary Mission and INSIS)

.. _`Contributors in OMEGAlpes repository`: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes/-/graphs/master
.. _`Apache 2.0`: https://www.apache.org/licenses/LICENSE-2.0.html
.. _`Creative Commons Attribution 4.0 International (CC BY 4.0)`: https://creativecommons.org/licenses/by/4.0/
.. _CDP Eco-SESA: https://ecosesa.univ-grenoble-alpes.fr/
.. _OMEGAlpes Gitlab: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes
.. _OMEGAlpes Examples Documentation: https://omegalpes_examples.readthedocs.io/
.. _OMEGAlpes interface: https://mhi-srv.g2elab.grenoble-inp.fr/OMEGAlpes-web-front-end/
.. _`AUTHORS in OMEGAlpes repository`: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes/-/blob/master/AUTHORS.rst