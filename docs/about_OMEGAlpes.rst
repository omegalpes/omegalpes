About OMEGAlpes
===============

OMEGAlpes is a linear optimization tool designed to easily generate multi-carrier energy system models. Its purpose is to assist in developing district energy projects by integrating design and operation in pre-studies phases.
OMEGAlpes is open-source and written in Python with the licence `Apache 2.0`_.
It is a generation model tool based on an intuitive and extensible object-oriented library that aims to provide a panel of pre-built energy units with predefined operational options, and associated constraints and objectives taking into account stakeholders.

OMEGAlpes is developed in order to provide various features regarding energy system optimization, and especially energy flexibility:

.. toctree::
   :maxdepth: 2

   api/flexibility

OMEGAlpes was also presented and used in several scientific publications that can be found here:

.. toctree::
   :maxdepth: 2

   api/publications

Partners:
---------
OMEGAlpes is mainly developped in the Grenoble Elctrical Engineering Laboratory (University Grenoble Alpes, CNRS, Grenoble INP, G2Elab, F-38000 Grenoble, France).

Other teams took part in the tool development:

- LOCIE (Laboratoire Optimisation de la Conception et Ingénierie de l’Environnement (LOCIE), CNRS UMR 5271—Université Savoie Mont Blanc, Polytech Annecy-Chambéry, Campus Scientifique, Savoie Technolac, CEDEX, 73376 Le Bourget-Du-Lac,) in thermal engineering for the exergy package in particular

- PACTE (Université Grenoble-Alpes, UMR 5194 PACTE) in social sciences for the actors package in particular

- MIAGE (Master Méthodes informatiques appliquées à la gestion des entreprises, Université Grenoble-Alpes) master students in computing for the GUI development.