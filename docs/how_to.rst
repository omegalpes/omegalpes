How to use OMEGAlpes
====================

*This page is under development and wil be updated*

OMEGAlpes use principles will be detailed here.

In the meantime, empty templates for creating OMEGAlpes models including
actors or not are available in this OMEGAlpes examples folder:
https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples/-/tree/master/case_study_template

A `tutorial`_ with linked `notebook`_ are also available.

The `notebook folder`_ also enable to discover OMEGAlpes functionnalities, and
especially `this notebook about waste heat recovery`_.

.. _tutorial: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples/-/blob/master/tutorials/Tutorial_OMEGAlpes_2020.md
.. _notebook: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples/-/blob/master/notebooks/To_Modify__PV_self_consumption_eng.ipynb
.. _notebook folder:https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples/-/tree/master/notebooks
.. _this notebook about waste heat recovery: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples/-/blob/master/notebooks/article_2021_MPDI_waste_heat.ipynb

