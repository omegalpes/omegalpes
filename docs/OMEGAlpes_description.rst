OMEGAlpes Structure
===================

The models are bases on **general**, **energy** and **actor** classes.
The structure of the library is described on the following class diagrams:

.. figure::  images/general_class_diagram.png
   :align:   center
   :scale:   40%

   *Figure: OMEGAlpes general class diagram*

.. figure::  images/energy_class_diagram.png
   :align:   center
   :scale:   40%

   *Figure: OMEGAlpes energy class diagram*

.. figure::  images/actor_class_diagram.png
   :align:   center
   :scale:   40%

   *Figure: OMEGAlpes actor class diagram*

The energy units are detailed in the energy package

.. toctree::
   :maxdepth: 2

   api/energy_package

The energy package also includes an exergy module

.. toctree::
   :maxdepth: 2

   api/exergy

The **actor classes** enables to build the energy model considering pre-defined
stakeholders' constraints and objectives

.. toctree::
   :maxdepth: 2

   api/actor

The **general classes** helps to build the units, the model and to plot the results

.. toctree::
   :maxdepth: 2

   api/general
